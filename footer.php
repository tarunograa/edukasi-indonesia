<style>
    <?php include('css/footer.css')  ?>
</style>

<footer>
    <div class="containerFooter">
        <div class="edin">
            <h2>EDIN</h2>
            <p class="kota">&copy;2020 Pulp Dev, Inc</p>
            <br><br>
            <p class="kota">Mataram, <br>Lombok Indonesia</p>
        </div>
        <div class="tentang">
            <ul>
                <a href="">
                    <li>Home</li>
                </a>
                <a href="">
                    <li>Courses</li>
                </a>
                <a href="">
                    <li>Blog</li>
                </a>
            </ul>
        </div>
        <div class="contact">
            <ul>
                <a href="">
                    <li>Contact</li>
                </a>
                <a href="">
                    <li>Term of Use</li>
                </a>
                <a href="">
                    <li>Privacy Policy</li>
                </a>
                <a href="">
                    <li>Faq</li>
                </a>
            </ul>
        </div>
    </div>

</footer>