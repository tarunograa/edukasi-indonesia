<?php
require 'ceklogin.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Materi Belajar</title>
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="../css/bootsrap/bootstrap.css">
    <link rel="stylesheet" href="../css/readmateri.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <?php include('../header.php')  ?>
    <main>
        <h1 class="mb-3">Biologi</h1>
        <div class="card text-white">
            <div class="card-header">
                <h4>Bab 1 Mengenal Biologi</h4>
                <i class="fa fa-chevron-down float-rgiht" aria-hidden="true"></i>
            </div>
            <div class="isi">

                <div class="card-body">
                    <h5 class="card-title">Biologi</h5>
                    <p class="card-text text-justify">Pada bab pertama ini, Anda akan belajar melakukan penelitian. Selain itu, Anda juga akan mempelajari tentang tahapan-tahapan penelitian ilmiah. Pada bagian lainnya, Anda akan mempelajari ruang lingkup Biologi, termasuk di dalamnya adalah mengenal cabang-cabang Biologi. Menarik, bukan?
                        Bagaimanakah cara melakukan penelitian yang baik? Tahapan-tahapan ilmiah apa saja yang harus dilakukan agar penelitian yang dilakukan berhasil dengan baik? Apakah yang dimaksud dengan Biologi? Apa sajakah cabang-cabang Biologi itu? Apakah peranan Biologi dalam kehidupan?
                        Pertanyaan-pertanyaan tersebut dapat Anda jawab setelah mempelajari dan memahami bab berikut dengan baik. Pelajarilah bab berikut dengan semangat dan rasa ingin tahu yang tinggi.<h5 class="card-title">A. Kerja Ilmiah</h5>Rasa ingin tahu manusia terhadap alam semesta yang diciptakan Tuhan dan kehidupan yang terdapat di dalamnya dari zaman dahulu hingga sekarang seakan tidak ada habis-habisnya. Persoalan dan permasalahan yang ada di alam pun seakan tidak akan pernah habis untuk digali. Itulah sebabnya, ilmu pengetahuan terus berkembang dan penemuan-penemuan baru pun terus bermunculan. Anda juga dapat ikut berperan dalam perkembangan ilmu pengetahuan tersebut jika mempunyai keinginan untuk terus menggali ilmu pengetahuan dan terus mencoba memuaskan rasa keingintahuan yang Anda miliki. 
Sampai saat ini banyak kasus mengenai penyakit yang sangat meresahkan masyarakat, yaitu flu burung. Pada mulanya apa yang menimbulkan penyakit ini tidak diketahui oleh masyarakat. Biologi mampu memecahkan masalah secara ilmiah, sehingga pada akhirnya dapat menjawab penyebab penyakit flu burung yang meresahkan tersebut, yaitu virus H5NI. Masih banyak hal lain yang dapat diselesaikan secara ilmiah, dapatkah Anda menyebutkan contoh lainnya? 
Dengan demikian, Anda mungkin dapat menjadi seorang penemu baru yang berguna bagi masyarakat luas. Hal tersebut dapat terjadi jika Anda memahami bagaimana kerja ilmiah yang dilakukan oleh para ilmuwan terdahulu dan mengaplikasikannya pada kegiatan-kegiatan ilmiah yang Anda lakukan. Untuk itu, hal pertama yang harus dilakukan adalah melakukan penelitian ilmiah. Apa saja tahapan untuk melakukan penelitian ilmiah? Berikut akan diuraikan bagaimana penelitian ilmiah dapat dilakukan.

                    </p>
                </div>
            </div>
        </div>
        <div class="card text-white">
            <div class="card-header">
                <h4>Bab 2 Pengenalan Materi</h4>
                <i class="fa fa-chevron-down float-rgiht" aria-hidden="true"></i>
            </div>
            <div class="isi">

                <div class="card-body">
                    <h5 class="card-title">Biologi</h5>
                    <p class="card-text text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolor delectus expedita optio sapiente, eos incidunt perspiciatis repudiandae eius quos dolorum velit accusamus similique consequatur ex repellat? Mollitia optio veritatis maxime quas exercitationem sint tenetur corrupti nisi sunt. Accusantium, aliquam rerum. Fugit dignissimos consectetur, repellat quas cupiditate optio voluptate incidunt sit? Accusantium cum mollitia pariatur quas ab consequuntur omnis possimus. Impedit nesciunt labore culpa at molestias delectus, quo tempore quod iusto repellat commodi sunt nulla dolor! Ipsa sapiente fugit, obcaecati sit praesentium soluta deserunt! Cum magni consectetur nobis molestias! Cupiditate dolor libero pariatur? Vel omnis, ipsam veritatis iusto quod porro necessitatibus.</p>
                </div>
            </div>
        </div>
    </main>
    <script src="../js/readmateri.js"></script>


</body>
<?php include('footer.php')  ?>

</html>